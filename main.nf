#!/usr/bin/env nextflow
/* 
======================================================================================
			Expansion Hunter Denovo - Nextflow Trial
======================================================================================
*/

/*
======================================================================================
======================================================================================
======				   Parameters					======
======================================================================================
======================================================================================
*/

def helpMessage() {

	println """
	Usage:

	--help				To Show this Message
	
	Required arguments:
    --ehd               Path to ExpansionHunter Denovo directory (do not include final 'bin' subdirectory)
	--reads				BAM/CRAM file with aligned reads [not fully implemented]
	OR
	--csv				CSV file with id, status (case, control or NA), 
                          file (bam or cram file) and index (corresponding index file)

	Optional arguments:
	--reference			FASTA file with reference genome
	--outdir			Directory to store results files
    --min-unit-len arg (=2)     Shortest repeat unit to consider (ExpansionHunter denovo parameter)
    --max-unit-len arg (=20)    Longest repeat unit to consider (ExpansionHunter denovo parameter)
    --min-anchor-mapq arg (=50) Minimum MAPQ of an anchor read (ExpansionHunter denovo parameter)
    --max-irr-mapq arg (=40)    Maximum MAPQ of an in-repeat read (ExpansionHunter denovo parameter)
	""".stripIndent()
}

params.help = false

// Show help message
if (params.help) {
	helpMessage()
	exit 0
}

/*
===================================================================
===================================================================
			Default Parameter values
===================================================================
*/

params.ehd = false
params.reads = false
params.csv = false  // CSV file should have id, file, index and case columns
params.reference = ""
params.outdir = "results/"
params.min_unit_len = 2
params.max_unit_len = 20
params.min_anchor_mapq = 50
params.max_irr_mapq = 40
params.name = "study"



/*
 *  Parse the input parameters
 */

if(params.ehd) {
    file("${params.ehd}/bin/ExpansionHunterDenovo", checkIfExists: true) 
    file("${params.ehd}/scripts/casecontrol.py", checkIfExists: true) 
    file("${params.ehd}/scripts/outlier.py", checkIfExists: true) 
    ehd_dir = file(params.ehd, checkIfExists: true)
} else {
    exit 1, "Please set directory of ExpansionHunter installation (do not include final 'bin')"
}
if(params.reference == "") {
    exit 1, "Please set --reference to a fasta file path."
}

ref_file = file(params.reference, checkIfExists: true)
ref_index = file("${ref_file.getParent()}/${ref_file.getName()}" + ".fai", checkIfExists: true)

//Channel.of (ref_file, ref_index).set {ref_file}
if(params.reads && params.csv){
	exit 1, "Specify either --reads or --csv not both!"
} else if(params.reads){
	reads_file = file(params.reads, checkIfExists: true)
	
	// if file does not exist exit with a message
	if (reads_file.isEmpty()) {exit 1, "BAM/CRAM file not found: ${params.reads}" }

	// Get the base name and file extension of the BAM/CRAM files 
	if (reads_file.getClass() == sun.nio.fs.UnixPath) {
		ext = (reads_file.getExtension() == "cram") ? ".crai" : ".bai"
		reads_index = "${reads_file.getParent()}/${reads_file.getBaseName()}.${reads_file.getExtension()}" + "${ext}"
		reads_index = file(reads_index, checkIfExists: true)

		// if file does not exist exit with a message
		if (reads_index.isEmpty()) {exit 1, "BAM/CRAM file not found: ${reads_index.getName()}" }

		Channel
			.of (reads_file.getBaseName(), reads_file, reads_index).collect()
			.set { reads_ch }
	} else {
		reads_index = []
		for (def index : reads_file) {
			ext = (index.getExtension() == "cram") ? ".crai" : ".bai"
			// if file does not exist exit with a message
			index_f = file("${index.getParent()}/${index.getBaseName()}.${index.getExtension()}" + "${ext}", checkIfExists: true)
			if (index_f.isEmpty()) {exit 1, "BAM/CRAM file not found: ${index_f.getName()}" }
			
			reads_index.add(tuple(index.getSimpleName(), index, index_f))
		}
		reads_ch = Channel.of ( reads_index ).flatten().collate(3)
	}
} else if (params.csv) {

    // sample aligned reads
	Channel
		.fromPath(params.csv, checkIfExists: true)
		.ifEmpty { exit 1, "No CSV file found under: ${params.csv}"}
		.splitCsv(header:true)
        .map { row -> tuple(row.id, file(row.file), file(row.index)) }
        .map { 
          if(it[0] == null) { exit 1, "Missing 'id' column in CSV file ${params.csv}." }; 
          if(it[0] == '') { exit 1, "'id' cannot be empty for any sample in CSV file ${params.csv}." }; 
          it 
        }
		.set { reads_ch }

    // sample status
	Channel
        .fromPath(params.csv, checkIfExists: true)
        .ifEmpty { exit 1, "No CSV file found under: ${params.csv}"}
        .splitCsv(header:true)
        .map { row -> tuple(row.id, row.status) }
        .map { if(it[1] == null) { exit 1, "Missing 'status' column in CSV file ${params.csv}." }; it }
        .set { status_ch }

    // set up status channels
    status_ch
        .map { if(! ['case', 'control', 'NA'].contains(it[1])) { exit 1, "Status ${it[1]} not allowed in CSV file ${params.csv}." } ; it }
        .into{ status_1_ch; status_2_ch }

    // status chanel for case and control (exclude anything else)
    status_1_ch
        .filter { it[1] == 'case' || it[1] == 'control' }
        .map { "${it[0]}\t${it[1]}\t${it[0]}.str_profile.json" }
        .reduce{a, b -> return "$a\n$b"}
        .set { status_cc_ch }

    // status chanel for outliers (set NA as controls, no discards)
    status_2_ch
        .map { if(it[1] == 'NA') { [it[0], 'control'] } else { it } }
        .map { "${it[0]}\t${it[1]}\t${it[0]}.str_profile.json" }
        .reduce{a, b -> return "$a\n$b"}
        .set { status_outlier_ch }
} else {
	exit 1, "Specify either --reads or --csv both!"
}

process 'str-profiles' {
	tag "$id"

	publishDir "${params.outdir}/str-profiles", mode: 'copy'
 
	input:
	file ref from ref_file
	file ref_i from ref_index
	tuple val(id), file(reads), file(reads_index) from reads_ch
	
	output:
	file("*") into ehd_json_channel
	
	script:
	"""
	$ehd_dir/bin/ExpansionHunterDenovo profile --reads "$reads" \
							 --reference "$ref" \
							 --output-prefix "${id}" \
                             --min-unit-len "${params.min_unit_len}" \
                             --max-unit-len "${params.max_unit_len}" \
                             --min-anchor-mapq  "${params.min_anchor_mapq}" \
                             --max-irr-mapq  "${params.max_irr_mapq}"
	"""
}

process 'create-manifest-cc' {
	tag "Creating manifest for case-control"

	publishDir "${params.outdir}/manifest", mode: 'copy'

	input:
	val(manifest_string) from status_cc_ch

	output:
	file("*") into manifest_cc_channel

	script:
	"""
	echo "$manifest_string" > "${params.name}.casecontrol.manifest.tsv"
	"""	
}


process 'create-manifest-outlier' {
	tag "Creating manifest for outliers"

	publishDir "${params.outdir}/manifest", mode: 'copy'

	input:
	val(manifest_string) from status_outlier_ch

	output:
	file("*") into manifest_outlier_channel

	script:
	"""
	echo "$manifest_string" > "${params.name}.outlier.manifest.tsv"
	"""	
}

manifest_cc_channel
  .into{ manifest_cc_channel_1; manifest_cc_channel_2 }

manifest_outlier_channel
  .into{ manifest_outlier_channel_1; manifest_outlier_channel_2 }

ehd_json_channel
    .collect()
    .into { ehd_json_cc_channel ; ehd_json_outlier_channel }


process 'merge-str-profiles-cc' {
	tag "Merging case-control"

	publishDir "${params.outdir}/merge", mode: 'copy'

	input:
	file ref from ref_file
	file ref_i from ref_index
	file manifest from manifest_cc_channel
    file("*") from ehd_json_cc_channel.collect()

	output:
	file("*") into merge_cc_channel

	script:
	"""
	$ehd_dir/bin/ExpansionHunterDenovo merge \
							--reference $ref \
							--manifest $manifest \
							--output-prefix ${params.name}.casecontrol
	"""	
}


process 'merge-str-profiles-outlier' {
	tag "Merging for outliers"

	publishDir "${params.outdir}/merge", mode: 'copy'

	input:
	file ref from ref_file
	file ref_i from ref_index
	file manifest from manifest_outlier_channel
    file("*") from ehd_json_outlier_channel.collect()

	output:
	file("*") into merge_outlier_channel

	script:
	"""
	$ehd_dir/bin/ExpansionHunterDenovo merge \
							--reference $ref \
							--manifest $manifest \
							--output-prefix ${params.name}.outlier
	"""	
}

process 'str-casecontrol' {
	tag "Case Control"

	publishDir "${params.outdir}/casecontrol", mode: 'copy'

	input:
	file manifest from manifest_cc_channel
	file merge from merge_cc_channel

	output:
	file("*") into cc_ch

	script:
	"""
	$ehd_dir/scripts/casecontrol.py motif \
						--manifest $manifest \
						--multisample-profile $merge \
						--output ${params.name}_motif_caseControl.tsv
	$ehd_dir/scripts/casecontrol.py locus \
						--manifest $manifest \
						--multisample-profile $merge \
						--output ${params.name}_locus_caseControl.tsv
	"""
}

// requires the latest numpy package
process 'str-outlier' {
        tag "Outlier"

        publishDir "${params.outdir}/outlier", mode: 'copy'

        input:
        file manifest from manifest_outlier_channel
        file merge from merge_outlier_channel

        output:
        file("*") into outlier_ch

        script:
        """
        $ehd_dir/scripts/outlier.py motif \
                                                --manifest $manifest \
                                                --multisample-profile $merge \
                                                --output ${params.name}_motif_outlier.tsv
        $ehd_dir/scripts/outlier.py locus \
                                                --manifest $manifest \
                                                --multisample-profile $merge \
                                                --output ${params.name}_locus_outlier.tsv
        """
}

