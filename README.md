# ExpansionHunterDenovo-nf

Nextflow pipeline for ExpansionHunter Denovo.

This pipeline runs ExpansionHunter Denovo profile on each sample. 
These profiles are then merged to run a case-control and outlier analysis. 

The pipeline uses a csv file for configuration.
Removing samples, or changes the case/control status of a sample
allow the use of cached profiles (with Nextflow `-resume`).
Only samples added will need to be profiled.

# Setup

## Requirements

This pipeline requires a unix-style system (tested in SUSE Linux Enterprise Server 12 SP3).

- [Nextflow](https://www.nextflow.io/) (tested with 19.10.0 build 5170)
- [Conda](https://docs.conda.io/en/latest/) (tested with 4.7.10)
- [ExpansionHunter Denovo](https://github.com/Illumina/ExpansionHunterDenovo) (tested with ExpansionHunterDenovo-v0.9.0-linux_x86_64)

A HPC queuing system may help. An example configuration for a Slurm system is included.
The test script setup instructions detail this further.

The pipeline may also run locally on a single server (standard profile). 
ExpansionHunter denovo is fast enough 
(~1 CPU hour per sample for profiling, and embarrassingly parallel for many samples) 
to run on a single server for a modest number of samples. 

## Test script

The test script runs the two example datasets from ExpansionHunter Denovo. 
These are included with ExpansionHunter Denovo and the test only needs to know the 
ExpansionHunter Denovo install location. 

First set the required values in `test.config`. 
This includes the path to the ExpansionHunter directory and 
extra command-line arguments to the nextflow script (such as job manager account details).

You will also need to configure `nextflow.config`. 
This file contains two profiles. 
The 'standard' profile runs the Nextflow script on the current machine, while
the 'zeus' profile is set up for a specific HPC system running the Slurm Workload Manager 
(Zeus is located at the Pawsey Supercomputing Centre, Perth WA, Australia). 
Profiles for other systems should be set up with the zeus profile as a guide and process
directives available for executor as documented in <https://www.nextflow.io/docs/latest/executor.html>.
Many of the directives are the same across different HPC executors, though the 
'queue' directive will likely need to be changed for your configuration. 

Run the test with
```{bash}
source test.sh
```

Please note that if the pipeline is run with one profile, then changed to another profile,
the pipeline stages may used cached results if the `-resume` option is included,
and is not a true test of the next profile. 
Nextflow will report when cached results are being used and for which stages. 
The `-resume` is useful in large pipelines where you only want to run pipeline jobs that have
not yet been completed. 
Either run without the `-resume` option, or delete the directory specified by `workDir` in `nextflow.config`.

# Running on your data

Create a csv file with at least these columns (with a header row) (in any order):

  - id: ID for downstream analysis. Should not include whitespace.
  - status: either 'case' or 'control'. The value doesn't matter if generating profiles to share with others. 
    Does not affect outlier analysis, though there must be at least one case and one control to avoid errors.
  - file: path to BAM file, preferably the full absolute path
  - index: path to BAM index file, preferably the full absolute path. (this is required to appropriately link the index file)

ExpansionHunter Denovo options may be set in the nextflow command for:

  - --min-unit-len arg (=2)     Shortest repeat unit to consider (ExpansionHunter denovo parameter)
  - --max-unit-len arg (=20)    Longest repeat unit to consider (ExpansionHunter denovo parameter)
  - --min-anchor-mapq arg (=50) Minimum MAPQ of an anchor read (ExpansionHunter denovo parameter)
  - --max-irr-mapq arg (=40)    Maximum MAPQ of an in-repeat read (ExpansionHunter denovo parameter)

I have found that data processed with Bowtie2 (with --very-sensitive-local mode) had few 
reads with mapping quality reaching over 50. As such, you should set --min_anchor_mapq lower.
I have had success with:
`--min_anchor_mapq=30`.
This may apply to other data aligned with different modes of Bowtie2.

You will also need an `fai` index to accompany your FASTA reference file. 
This can be created with samtools (faidx command).

Parameters to nextflow may be specified either at the command line or in `nextflow.config`, 
with the command line options taking priority.
```
export REFERENCE="/data_common/GRCh38/GCA_000001405.15_GRCh38_no_alt_analysis_set.fna"

nextflow run main.nf \
    --csv manifest.csv \
    --name mystudy \
    --outdir="results_mystudy" \
    --reference="$REFERENCE" \
    -profile standard \
    -resume
```

# Contributors

This Nextflow pipeline was authored by Joseph Sigar and Rick Tankard. 
