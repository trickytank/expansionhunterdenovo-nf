#!/bin/bash -l

## ---- set these parameters ---- ##

# Path to ExpansionHunter Denovo install directory
export EHD="/group/pawsey0306/rtankard/software/ExpansionHunterDenovo-v0.9.0-linux_x86_64"

# Nextflow run command line
NEXTFLOW_OPTIONS="-profile standard"


## ---- Testing ---- ##

# Prepare test csv for case-control example
cat <(echo "id,status,file,index") \
    <(perl -a -wne '$bam = "$ENV{EHD}/examples/case-control/bamlets/$F[0].bam"; print "$F[0],$F[1],$bam,$bam.bai\n"' "$EHD/examples/case-control/manifest.tsv") \
    > test_case-control.csv

# Prepare test csv for outlier example
cat <(echo "id,status,file,index") \
    <(perl -a -wne '$bam = "$ENV{EHD}/examples/outlier/bamlets/$F[0].bam"; print "$F[0],$F[1],$bam,$bam.bai\n"' "$EHD/examples/outlier/manifest.tsv") \
    > test_outlier.csv

# Run the nextflow
# For Bowtie2, use --min_anchor_mapq=30 (there may be better values for this, but the default of 50 is too high and will give almost no results)
nextflow run main.nf \
    $NEXTFLOW_OPTIONS \
    --outdir="results_test_case-control" \
    --name="ExpansionHunterDenovo_example" \
    --csv test_case-control.csv \
    -resume
    #--ehd "$EHD" \
    #--reference="$EHD/examples/case-control/reference.fasta" \


nextflow run main.nf \
    $NEXTFLOW_OPTIONS \
    --outdir="results_test_outlier" \
    --name="ExpansionHunterDenovo_example" \
    --csv test_outlier.csv \
    --reference="$EHD/examples/outlier/reference.fasta" \
    -resume

